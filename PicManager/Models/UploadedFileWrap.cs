﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PicManager.Models
{
    // "Обертка" для хранения данных о подгруженном изображении
    public class UploadedFileWrap
    {
        [Required]
        public HttpPostedFileBase UploadedFile { get; set; } // загруженный файл

        [Required]
        public string FileName { get; set; } // имя файла (введенное вручную)

        public string FileExtension { get; set; } // расширение файла

        public string FilePath { get; set; } // путь к файлу (для отображения и хранения в БД)
    }
}