﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicManager.Models
{
    interface IAlbumManager
    {
        // Проверка на содержимое БД
        bool IsEmpty();

        // Получение всех изображений из БД
        Image[] GetImages();

        // Добавление записи об изображении в БД
        void AddImage(string iName, string iExt, string iPath);

        // Удаление записи об изображении в БД
        void RemoveImage(string iName, string iId);

    }
}
