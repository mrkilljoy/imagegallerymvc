﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace PicManager.Models
{
    public class OrdinaryAlbumManager : IAlbumManager
    {
        public OrdinaryAlbumManager()
        {}

        public Image[] GetImages()
        {
            using (var cont = new AlbumContext())
            {
                List<object> img_list = new List<object>();
                var result = cont.Set<Image>().ToArray();

                if (result.Count() == 0 || result.Any() == false)
                    return null;

                return result.ToArray();
            }
        }

        public void AddImage(string iName, string iExt, string iPath)
        {
            using (var cont = new AlbumContext())
            {
                cont.Set<Image>().Add(new Image { Filename = iName, Extension = iExt, Filepath = iPath });
                cont.SaveChanges();
            }
        }

        public void RemoveImage(string iName, string iId)
        {
            using (var cont = new AlbumContext())
            {
                int id = Convert.ToInt32(iId);
                var found = from img in cont.Set<Image>() where img.Id == id && img.Filename == iName select img;

                if (found.Count() == 0 || found.Any() == false)
                    return;
                else
                {
                    cont.Set<Image>().Remove(found.First());
                    cont.SaveChanges();
                }
            }
        }

        public bool IsEmpty()
        {
            using (var cont = new AlbumContext())
            {
                int imgcount = cont.Images.Count();

                if (imgcount > 0)
                    return false;
                else
                    return true;
            }
        }
    }
}