namespace PicManager
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AlbumContext : DbContext
    {
        public AlbumContext()
            : base("name=AlbumContext")
        {
        }

        public virtual DbSet<Image> Images { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>()
                .Property(e => e.Extension)
                .IsUnicode(false);
        }
    }
}
