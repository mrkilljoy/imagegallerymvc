namespace PicManager
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Image
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Filename { get; set; }

        [Required]
        public string Filepath { get; set; }

        [Required]
        [StringLength(10)]
        public string Extension { get; set; }
    }
}
