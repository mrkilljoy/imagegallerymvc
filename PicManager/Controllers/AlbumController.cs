﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PicManager.Models;
using System.IO;

namespace PicManager.Controllers
{
    public class AlbumController : Controller
    {
        // для операций с БД
        IAlbumManager mng;

        [HttpGet]
        public ActionResult Images()
        {
            return View(new UploadedFileWrap());
        }

        [HttpPost]
        public ActionResult Images(UploadedFileWrap fw, string fId, string fName, string fPath)
        {
            if (ModelState.IsValid)
            {
                UploadImage(fw);
                return View(new UploadedFileWrap());
            }
            else
            {
                return View(new UploadedFileWrap());
            }
        }

        // Метод для возвращения partialview с содержимым галереи
        public ActionResult AlbumContent()
        {
            mng = new OrdinaryAlbumManager();
            if (mng.IsEmpty())
                return Content("Album is empty. You can upload something!");
            else
            {
                var imgs = mng.GetImages();
                return PartialView("AlbumContent",imgs);
            }
        }

        [HttpPost]
        public ActionResult RemoveImage(string fId, string fName, string fPath)
        {
            mng = new OrdinaryAlbumManager();
            var actualpath = Server.MapPath(fPath); // физическое расположение картинки

            if (System.IO.File.Exists(actualpath))
            {
                mng.RemoveImage(fName, fId);
                System.IO.File.Delete(actualpath);
            }

            return RedirectToAction("Images");
        }

        // Загрузка изображения на сервер
        private void UploadImage(UploadedFileWrap fw)
        {
            mng = new OrdinaryAlbumManager();

            fw.FileExtension = GetFileExtension(fw);

            // костыль для проверки расширения загружаемого файла
            if (fw.FileExtension == ".jpg" || fw.FileExtension == ".png" || fw.FileExtension == ".gif" || fw.FileExtension == ".jpeg")
            {
                fw.FilePath = string.Format("/Content/usr_pics/{0}{1}", fw.FileName, fw.FileExtension);

                string actualpath = Server.MapPath(fw.FilePath); // физическое расположение картинки
                fw.UploadedFile.SaveAs(actualpath);
                mng.AddImage(fw.FileName, fw.FileExtension, fw.FilePath);
            }
        }

        // Удаление изображения с сервера (БД и папки)
        private void DeleteImage(string fId, string fName, string fPath)
        {
            mng = new OrdinaryAlbumManager();
            var actualpath = Server.MapPath(fPath);

            if (System.IO.File.Exists(actualpath))
            {
                mng.RemoveImage(fName, fId);
                System.IO.File.Delete(actualpath);
            }
        }

        // Получение расширения загруженного файла
        private string GetFileExtension(UploadedFileWrap fWrap)
        {
            string ctype = fWrap.UploadedFile.ContentType;
            int sep_pos = ctype.IndexOf('/');
            string ext = "." + ctype.Substring(sep_pos + 1);
            return ext;
        }
    }
}